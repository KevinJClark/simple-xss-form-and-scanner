<?php
if (isset($_POST["password"]) && !empty($_POST["password"])) {
	echo "Username: ";
	echo '<!--VULNERABLE to XSS--> ';
	echo $_POST["username"];
	echo "\n<br>\n";
	echo "Password: ";
	echo '<!--NOT vulnerable to XSS--> ';
	echo htmlspecialchars($_POST["password"]);
}

if (isset($_GET["password"]) && !empty($_GET["password"])) {
	echo "Username2: ";
	echo '<!--NOT vulnerable to XSS--> ';
	echo htmlspecialchars($_GET["username"]);
	echo "\n<br>\n";
	echo "Password2: ";
	echo '<!--VULNERABLE to XSS--> ';
	echo $_GET["password"];
}
?>
